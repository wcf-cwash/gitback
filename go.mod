module gitback

go 1.16

require (
	github.com/go-git/go-git/v5 v5.3.0
	github.com/xanzy/go-gitlab v0.50.0
)
