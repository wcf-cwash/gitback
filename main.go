package main

import (
	"flag"
	"gitback/config"
	"gitback/info"
	"gitback/logging"
	"gitback/utils"
	"github.com/go-git/go-git/v5"
	"github.com/go-git/go-git/v5/plumbing/transport/http"
	"github.com/xanzy/go-gitlab"
	"os"
	"path/filepath"
	"strings"
)

/* These are default ldflag fields set to main.* by goreleaser, see default ldflags */
/* https://goreleaser.com/customization/build/ */
var version string
var commit string
var date string
var builtBy string

// GlobalConfig will hold the global configuration state for the program.
var GlobalConfig config.Config

type invalidArgsError struct {
	Message string
}

func (e invalidArgsError) Error() string {
	return e.Message
}

func main() {

	performSetup()

	printer := info.Printer{GitbackConfig: GlobalConfig}
	printer.Info()

	if _, err := os.Stat(GlobalConfig.BackupDir); os.IsNotExist(err) {
		os.Mkdir(GlobalConfig.BackupDir, 0755)
	}

	gitlabToken := GlobalConfig.FileConfig.Gitlab.Token
	if gitlabToken == "" {
		logging.Error.Fatalf("No Gitlab.Token set in Configuration File; unable to proceed")
	}

	gitlabDotCom, err := gitlab.NewClient(gitlabToken)
	if err != nil {
		logging.Error.Fatalf("Failed to create client: %v", err)
	}

	groups, _, err := gitlabDotCom.Groups.ListGroups(&gitlab.ListGroupsOptions{
		ListOptions: gitlab.ListOptions{
			Page:    1,
			PerPage: 1000,
		},
	})
	CheckIfError(err)

	for _, group := range groups {

		groupName := group.Name
		groupGuid := group.ID

		groupProjects, _, err := gitlabDotCom.Groups.ListGroupProjects(groupGuid, &gitlab.ListGroupProjectsOptions{})
		CheckIfError(err)

		for _, project := range groupProjects {

			projectName := project.Name
			projectPath := project.PathWithNamespace
			repoUrl := project.HTTPURLToRepo

			path := filepath.Join(GlobalConfig.BackupDir, projectPath)
			if _, err := os.Stat(path); !os.IsNotExist(err) {
				logging.Info.Printf("Updating %s/%s\n", groupName, projectName)

				// We instantiate a new repository targeting the given path (the .git folder)
				r, err := git.PlainOpen(path)

				if err != nil && (err.Error() == "remote repository is empty" || err.Error() == "repository does not exist") {
					logging.Error.Printf("\x1b[31;1m%s\x1b[0m\n", err)
					continue
				} else {
					CheckIfError(err)
				}

				// Get the working directory for the repository
				w, err := r.Worktree()
				CheckIfError(err)

				// Pull the latest changes from the origin remote and merge into the current branch
				err = w.Pull(&git.PullOptions{RemoteName: "origin"})
				CheckIfError(err)

			} else {
				cloneProject(groupName, projectName, path, repoUrl, gitlabToken)

			}

		}

	}
}

func cloneProject(groupName string, projectName string, path string, repoUrl, token string) {
	logging.Info.Printf("Cloning %s/%s\n", groupName, projectName)
	os.Mkdir(path, 0755)

	auth := &http.BasicAuth{"token", token}

	_, err := git.PlainClone(path, false, &git.CloneOptions{
		URL:      repoUrl,
		Auth:     auth,
		Progress: os.Stdout,
	})
	CheckIfError(err)
}

func performSetup() {
	// parse CLI arguments
	GlobalConfig = parseArguments()
	// initialize logging resources
	logging.InitLogging(GlobalConfig)
	// print version if version or debug flag set
	info.GitbackVersion(GlobalConfig)
}

func parseArguments() config.Config {

	versionFlagPtr := flag.Bool("version", false, "Print version")
	debugFlagPtr := flag.Bool("debug", false, "Run in debug mode")
	silentFlagPtr := flag.Bool("silent", false, "Run in silent mode")

	var fileConfigLocation string
	flag.StringVar(&fileConfigLocation, "config", "", "Change the configuration file location")

	var backupDirString string
	flag.StringVar(&backupDirString, "backup-dir", "", "Change the backup directory location")

	var regionString string
	flag.StringVar(&regionString, "region", "", "Specify the AWS region to use")

	flag.Parse()

	utils.Check(invalidArgs(debugFlagPtr, silentFlagPtr))

	var gitbackConfig = config.New()

	if *debugFlagPtr {
		gitbackConfig.DebugMode = true
		gitbackConfig.SilentMode = false
	} else if *silentFlagPtr {
		gitbackConfig.DebugMode = false
		gitbackConfig.SilentMode = true
	}

	if *versionFlagPtr {
		gitbackConfig.PrintVersion = true
	}

	if fileConfigLocation != "" {
		gitbackConfig.FileConfigLocation = fileConfigLocation
	}

	fileConfig, err := config.ParseConfig(gitbackConfig.FileConfigLocation)
	if err != nil {
		logging.Error.Fatalf("Problem loading configuration file[%s]: %s", gitbackConfig.FileConfigLocation, err)
	}
	gitbackConfig.FileConfig = fileConfig

	if backupDirString != "" {
		gitbackConfig.BackupDir = backupDirString
	}

	if regionString != "" {
		gitbackConfig.Region = regionString
	}

	/* build related fields */

	if version != "" {
		gitbackConfig.BuildVersion = version
	}

	if commit != "" {
		gitbackConfig.BuildCommit = commit
	}

	if date != "" {
		gitbackConfig.BuildDate = date
	}

	if builtBy != "" {
		gitbackConfig.BuiltBy = builtBy
	}

	return gitbackConfig

}

func invalidArgs(debugFlagPtr *bool, silentFlagPtr *bool) error {
	if *debugFlagPtr && *silentFlagPtr {
		return &invalidArgsError{
			Message: "Debug and Silent modes are mutually exclusive, please set only one",
		}
	}

	return nil
}

// CheckArgs should be used to ensure the right command line arguments are
// passed before executing an example.
func CheckArgs(arg ...string) {
	if len(os.Args) < len(arg)+1 {
		logging.Warning.Printf("Usage: %s %s", os.Args[0], strings.Join(arg, " "))
		os.Exit(1)
	}
}

// CheckIfError should be used to naively panics if an error is not nil.
func CheckIfError(err error) {
	if err == nil {
		return
	}

	logging.Warning.Printf("error: %s", err)
}
