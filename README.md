# gitback

Backup all Gitlab projects, upload to S3

## Configuration

### Set Your Access Token

[Create a Personal Access Token](https://gitlab.com/-/profile/personal_access_tokens) (
see [docs](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html)) with the following scopes:

* `api`
* `read_user`
* `read_api`
* `read_repository`

Drop this value into a `gitback.conf` file (or other file with appropriate least-privilege permissions)

```bash
touch gitback.conf
chmod 600 gitback.conf
vim gitback.conf
```

Add the following content into the file, replacing with your access token:

```json
{
  "Gitlab": {
    "Token": "<your-access-token-here>"
  }
}
```

Make sure you don't check this file into version control or anything.

By default, the app will look for this file as `gitback.conf` in the local directory, but if you'd like to override
where this lives you can pass the flag `-config` to override the location.

### Backup Directory

By default the program will create a local directory `./repos_backup` for cloning/updating repositories. You can
override this using the flag `-backup-dir`.

## goreleaser

### Initial Setup

* Add `.gitlab-ci.yml` file, and `.goreleaser.yml` files
* [Create a Personal Access Token](https://gitlab.com/-/profile/personal_access_tokens) with the `api` scope
* Add the token as a variable called `GITLAB_TOKEN` using to the "Variables" section of
  the [Settings/CICD page](https://gitlab.com/wcf-cwash/gitback/-/settings/ci_cd)
* Add the pattern `v*` to the "Protected tags" section of
  the [Settings/Repository page](https://gitlab.com/wcf-cwash/gitback/-/settings/repository)

### Cutting a release

Create a new tag via the [Tags](https://gitlab.com/wcf-cwash/gitback/-/tags) page with the naming convention of `v*.*.*`
, following [Semantic Versioning](https://semver.org) spec