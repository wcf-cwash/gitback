package utils

import (
	"gitback/logging"
	"net/http"
	"regexp"
)

// Check will see if an error is present, log and then exit with non-zero status.
func Check(err error) {
	if err != nil {
		logging.Error.Fatalln(err)
	}
}

// Ignore will log a warning of any error present and continue
func Ignore(err error) {
	if err != nil {
		logging.Debug.Println("Ignoring ", err)
	}
}

// CloseBody will do error handling for closing a response, defer this instead of resp.Body.Close()
func CloseBody(resp *http.Response) {
	closeErr := resp.Body.Close()
	if closeErr != nil {
		logging.Error.Fatalln(closeErr)
	}

}

// GetParams parses url with the given regular expression and returns the group values defined in the expression
func GetParams(compRegEx *regexp.Regexp, url string) (paramsMap map[string]string) {

	match := compRegEx.FindStringSubmatch(url)

	paramsMap = make(map[string]string)
	for i, name := range compRegEx.SubexpNames() {
		if i > 0 && i <= len(match) {
			paramsMap[name] = match[i]
		}
	}
	return
}
