package config

import (
	"encoding/json"
	"log"
	"os"
)

type FileConfig struct {
	Gitlab struct {
		Token    string
		GroupUrl string
	}
	// AWS/S3...
}

func ParseConfig(fileConfigLocation string) (*FileConfig, error) {

	file, err := os.Open(fileConfigLocation)
	if err != nil {
		return nil, err
	}

	defer func(file *os.File) {
		err := file.Close()
		if err != nil {
			log.Fatalf("Can't close config file[%s]: %s", fileConfigLocation, err)
		}
	}(file)

	decoder := json.NewDecoder(file)

	fileConfig := FileConfig{}
	err = decoder.Decode(&fileConfig)
	if err != nil {
		return nil, err
	}

	return &fileConfig, nil
}
