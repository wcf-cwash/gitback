package config

// Config encapsulates important state obtained from env vars and/or cmd line arguments
// and can be passed around to other parts of the application that are configurable
type Config struct {
	DebugMode          bool
	SilentMode         bool
	PrintVersion       bool
	FileConfigLocation string
	FileConfig         *FileConfig
	BackupDir          string
	Region             string
	BuildVersion       string
	BuildCommit        string
	BuildDate          string
	BuiltBy            string
}

// New will initialize a new Config with defaults
func New() Config {
	return Config{
		DebugMode:          false,
		SilentMode:         false,
		PrintVersion:       false,
		FileConfigLocation: "./gitback.conf",
		BackupDir:          "./repos_backup",
		Region:             "us-east-1",
		BuildVersion:       "n/a",
		BuildCommit:        "n/a",
		BuildDate:          "n/a",
		BuiltBy:            "n/a",
	}
}
