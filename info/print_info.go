package info

import (
	"gitback/config"
	"gitback/logging"
)

type Printer struct {
	GitbackConfig config.Config
}

func (p *Printer) Info() {
	if !p.GitbackConfig.SilentMode {
		logging.Message.Println()
		logging.Message.Println("--------------------------------------------------------------------------------------------------")
		logging.Message.Println(" _________________ ")
		logging.Message.Println("|# :           : #|")
		logging.Message.Println("|  :           :  |   _____ _____ ___________  ___  _____  _   __")
		logging.Message.Println("|  :           :  |  |  __ \\_   _|_   _| ___ \\/ _ \\/  __ \\| | / /")
		logging.Message.Println("|  :           :  |  | |  \\/ | |   | | | |_/ / /_\\ \\ /  \\/| |/ / ")
		logging.Message.Println("|  :___________:  |  | | __  | |   | | | ___ \\  _  | |    |    \\ ")
		logging.Message.Println("|     _________   |  | |_\\ \\_| |_  | | | |_/ / | | | \\__/\\| |\\  \\")
		logging.Message.Println("|    | __      |  |   \\____/\\___/  \\_/ \\____/\\_| |_/\\____/\\_| \\_/")
		logging.Message.Println("|    ||  |     |  |")
		logging.Message.Println("\\____||__|_____|__|")
		logging.Message.Println("--------------------------------------------------------------------------------------------------")
		logging.Message.Println("Gitback!")
		logging.Message.Println("--------------------------------------------------------------------------------------------------")
	}
}

