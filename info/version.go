package info

import (
	"gitback/config"
	"gitback/logging"
)

// GitbackVersion prints the current Gitback version when version flag or debug flag is set
func GitbackVersion(config config.Config) {

	if config.PrintVersion || config.DebugMode {
		logging.Message.Printf("Gitback version %s (Commit %s) \nBuilt on %s by %s",
			config.BuildVersion,
			config.BuildCommit,
			config.BuildDate,
			config.BuiltBy)
	}
}
