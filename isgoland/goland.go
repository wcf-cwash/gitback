// +build goland

package isgoland

// Enabled is a build flag that is used to work-around the fact that the echo/off library we use
// for reading passwords from StdIn does not work inside of Goland.
const Enabled = true
